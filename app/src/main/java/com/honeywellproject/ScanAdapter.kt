package com.honeywellproject

import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.rv_scan_layout.view.*
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.time.temporal.TemporalQueries.localDate




class ScanAdapter(val items : ArrayList<ScanObject>, val context: Context) : RecyclerView.Adapter<ViewHolder>() {


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.txtparcelid.text=items[position].parcelid

        val date = items[position].date

        val sdf = SimpleDateFormat("dd/MM/yyyy")
        val sdftime = SimpleDateFormat("hh:mm aa")


       val formattedTime=sdftime.format(date)
        
       val formattedDate=sdf.format(date)
          
        holder.txttime.text=formattedTime

        holder.txtdate.text=formattedDate
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.rv_scan_layout, parent, false))
    }


    // Gets the number of animals in the list
    override fun getItemCount(): Int {
        return items.size
    }


}



class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {

   val txtparcelid = view.txtparcelid
    val txttime=view.txttime
    val txtdate=view.txtdate
}


