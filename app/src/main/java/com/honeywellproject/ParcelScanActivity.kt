package com.honeywellproject

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.honeywellproject.Utils.MyApplication
import com.honeywellproject.Utils.isNetworkAvailable
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.RequestParams
import com.loopj.android.http.TextHttpResponseHandler
import cz.msebera.android.httpclient.Header
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.activity_parcel_scan.*
import org.json.JSONArray
import org.json.JSONObject
import spencerstudios.com.bungeelib.Bungee
import java.text.SimpleDateFormat
import java.util.*

class ParcelScanActivity : AppCompatActivity() {

    var sharedPreferences = MyApplication.instance!!.getSharedPreferences("ryanair", Context.MODE_PRIVATE)
    var editor: SharedPreferences.Editor = sharedPreferences!!.edit()

    val barcodeList: ArrayList<ScanObject> = ArrayList()

    val user_id= sharedPreferences.getString("user_id","")
    val location= sharedPreferences.getString("location","")
    lateinit var scanAdapter : ScanAdapter

    var jsonArrar=JSONArray()

    var total:Int=0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_parcel_scan)

        //addBarcode()

        scanAdapter=ScanAdapter(barcodeList, this)
        recycleviewScanQR.adapter= scanAdapter

        etparcel.requestFocus()

        etparcel.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable) {

                if(s!=null && !s.toString().equals("") ){

                   addBarcode(s.toString())

                }



            }
            override fun beforeTextChanged(s:CharSequence, start:Int,
                                           count:Int, after:Int) {}
            override fun onTextChanged(s:CharSequence, start:Int,
                                       before:Int, count:Int) {}
        })

        linear_submit.setOnClickListener {
            submitData()
        }

        etparcel.showSoftInputOnFocus = false

        etparcel.setOnClickListener{
            etparcel.showSoftInputOnFocus = true
        }
    }

    private fun submitData() {


        if(total>0){

            if(isNetworkAvailable(this@ParcelScanActivity))
            {


                uploadData()
            }
            else{
                storeDataLocally()
            }
        }
        else{
            Toasty.error(MyApplication.instance, "Please Scan a QR code first", Toasty.LENGTH_SHORT).show()
        }




    }

    private fun storeDataLocally() {

        var data = sharedPreferences.getString("data", "")


        var count= barcodeList.size

        if(data.equals("") ){


            for (i in 0 until count){

                val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

                var jsonObject=JSONObject()
                jsonObject.put("barcode_id", barcodeList[i].parcelid)
                jsonObject.put("time",sdf.format( barcodeList[i].date))
                jsonObject.put("user_id",user_id)
                jsonObject.put("location_id", location)
                jsonArrar.put(jsonObject)
            }

            editor.putString("data", jsonArrar.toString())
            editor.apply()
        }
        else{

            var temparr=JSONArray(data)

            var count= barcodeList.size

            for (i in 0 until count){

                val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

                var jsonObject=JSONObject()
                jsonObject.put("barcode_id", barcodeList[i].parcelid)
                jsonObject.put("time",sdf.format( barcodeList[i].date))
                jsonObject.put("user_id",user_id)
                jsonObject.put("location_id", location)
                temparr.put(jsonObject)
            }

            editor.putString("data", temparr.toString())
            editor.apply()


        }


        Toasty.warning(MyApplication.instance,"Internet not available Data will be Uploaded Later", Toast.LENGTH_LONG).show()

        finish()


    }

    private fun uploadData() {

        var count= barcodeList.size

        for (i in 0 until count){

            val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

            var jsonObject=JSONObject()
            jsonObject.put("barcode_id", barcodeList[i].parcelid)
            jsonObject.put("time",sdf.format( barcodeList[i].date))
            jsonObject.put("user_id",user_id)
            jsonObject.put("location_id", location)
            jsonArrar.put(jsonObject)
        }

        val params = RequestParams()
        params.put("data", jsonArrar)


        val client = AsyncHttpClient()
        client.post("http://portfolio.theaxontech.com/CI/kstronic/AddData", params, object : TextHttpResponseHandler() {

            override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseString: String?) {


                Log.e("response", responseString)

                Toasty.success(this@ParcelScanActivity, "Upload Sucessfull", Toast.LENGTH_LONG).show()
                finish()

            }

            override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseString: String?, throwable: Throwable?) {
                Log.e("responseError", responseString)
                Toasty.error(this@ParcelScanActivity, "Upload Error", Toast.LENGTH_LONG).show()

            }

            override fun onRetry(retryNo: Int) {

            }
        })
    }


    fun addBarcode(parcelid: String) {

        var exists=barcodeList.any{scanObject: ScanObject -> scanObject.parcelid==parcelid }
        if(!exists){
            barcodeList.add(ScanObject(parcelid, Calendar.getInstance().time))
            total= barcodeList.size
            txtTotal.text = total.toString()
            scanAdapter.notifyDataSetChanged()
        }
        else{Toasty.error(MyApplication.instance, "Parcel already exists",Toast.LENGTH_SHORT).show()
            }
        etparcel.text.clear()

//        val imm: InputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
//        if (imm.isActive)
//            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)

    }

}
