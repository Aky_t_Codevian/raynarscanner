package com.honeywellproject

import java.util.*

data class ScanObject (
        val parcelid: String,
        val date: Date
)