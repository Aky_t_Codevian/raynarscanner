package com.honeywellproject

import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import com.honeywellproject.Utils.MyApplication
import kotlinx.android.synthetic.main.activity_login.*
import spencerstudios.com.bungeelib.Bungee
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.TextHttpResponseHandler
import cz.msebera.android.httpclient.Header
import com.loopj.android.http.RequestParams
import es.dmoral.toasty.Toasty
import org.json.JSONObject


class LoginActivity : AppCompatActivity() {

    var username: String =""
    var sharedPreferences = MyApplication.instance!!.getSharedPreferences("ryanair", Context.MODE_PRIVATE)
    var editor: SharedPreferences.Editor = sharedPreferences!!.edit()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)



        et_username.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable) {

                if(s!=null && !s.toString().equals("")){
                    username=s.toString()
                    loginUser()

                }



            }
            override fun beforeTextChanged(s:CharSequence, start:Int,
                                           count:Int, after:Int) {}
            override fun onTextChanged(s:CharSequence, start:Int,
                                       before:Int, count:Int) {}
        })


        linear_login.setOnClickListener{
            if(username.equals("")){
                Toast.makeText(this@LoginActivity, "User ID is blank", Toast.LENGTH_LONG).show()
            }
            else{
                loginUser()
            }


        }
    }

    fun loginUser(){


        val params = RequestParams()
        params.put("user_name", username)


        val client = AsyncHttpClient()
        client.post("http://portfolio.theaxontech.com/CI/kstronic/LoginApp", params, object : TextHttpResponseHandler() {

            override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseString: String?) {


                    var jsonObject=JSONObject(responseString)

                var user_id=jsonObject.getString("data")

                Toasty.success(this@LoginActivity, "Login Sucessfull", Toast.LENGTH_LONG).show()

                editor.putString("username", username)
                editor.putString("user_id", user_id)
                editor.commit()
                var intent= Intent(this@LoginActivity, HomeActivity::class.java)
                startActivity(intent)
                Bungee.fade(this@LoginActivity)
                finish()
            }

            override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseString: String?, throwable: Throwable?) {
                Toasty.error(this@LoginActivity, "Invalid UserID", Toast.LENGTH_LONG).show()

            }

            override fun onRetry(retryNo: Int) {

            }
        })



    }
}
