package com.honeywellproject

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.honeywellproject.Utils.MyApplication
import kotlinx.android.synthetic.main.activity_home.*


class HomeActivity : AppCompatActivity() {

    var sharedPreferences = MyApplication.instance!!.getSharedPreferences("ryanair", Context.MODE_PRIVATE)
    var editor: SharedPreferences.Editor = sharedPreferences!!.edit()

    var username : String = ""
    var location : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        username=sharedPreferences.getString("username", "")
        location=sharedPreferences.getString("location", "")

        txtusername.setText(username)
        txtlocation.setText(location)

        cardLocation.setOnClickListener {
            var intent = Intent(this@HomeActivity, ScanLocationActivity::class.java)
            startActivity(intent)

        }

        cardParcelScan.setOnClickListener {
            var intent = Intent(this@HomeActivity, ParcelScanActivity::class.java)
            startActivity(intent)
        }

        cardLogout.setOnClickListener {

            editor.remove("username")
            editor.remove("location")
            editor.remove("user_id")
            editor.apply()

            var intent = Intent(this@HomeActivity, LoginActivity::class.java)
            startActivity(intent)
            finish()

        }


    }

    override fun onResume() {
        super.onResume()
        location=sharedPreferences.getString("location", "")
        txtlocation.setText(location)

    }
}
