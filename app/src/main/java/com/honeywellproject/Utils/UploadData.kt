package com.honeywellproject.Utils

import android.app.IntentService
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.util.Log
import android.widget.Toast
import com.loopj.android.http.RequestParams
import com.loopj.android.http.SyncHttpClient
import com.loopj.android.http.TextHttpResponseHandler
import cz.msebera.android.httpclient.Header
import es.dmoral.toasty.Toasty
import org.json.JSONArray

class UploadData : IntentService(UploadData::class.simpleName) {
    override fun onHandleIntent(intent: Intent?) {

        var sharedPreferences = MyApplication.instance!!.getSharedPreferences("ryanair", Context.MODE_PRIVATE)
        var editor: SharedPreferences.Editor = sharedPreferences!!.edit()


        var data= sharedPreferences.getString("data", "")

        if(!data.equals("")){

            var jsonArrar=JSONArray(data)

            val params = RequestParams()
            params.put("data", jsonArrar)


            val client = SyncHttpClient()
            client.post("http://portfolio.theaxontech.com/CI/kstronic/AddData", params, object : TextHttpResponseHandler() {

                override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseString: String?) {


                    Log.e("response", responseString)

                    Toasty.success(MyApplication.instance, "Upload Sucessfull", Toast.LENGTH_LONG).show()
                    editor.remove("data")
                    editor.commit()

                }

                override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseString: String?, throwable: Throwable?) {
                    Log.e("responseError", responseString)
                    Toasty.error(MyApplication.instance, "Invalid UserID", Toast.LENGTH_LONG).show()

                }

                override fun onRetry(retryNo: Int) {

                }
            })
        }





    }
}