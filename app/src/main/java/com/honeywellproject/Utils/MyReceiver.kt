package com.honeywellproject.Utils

import android.widget.Toast
import android.content.Intent
import android.content.BroadcastReceiver
import android.content.Context
import android.util.Log


class MyReceiver : BroadcastReceiver() {

  override  fun onReceive(context: Context, intent: Intent) {

       // Toast.makeText(MyApplication.instance, "Action: " + intent.action!!, Toast.LENGTH_SHORT).show()

        Log.e("NetworkChange ", "Detected")

      if(isNetworkAvailable(context))
      {
          //Toast.makeText(context, "Network Available Do operations",Toast.LENGTH_LONG).show()
          var intent=Intent(context, UploadData::class.java)
          context.startService(intent)
      }
    }


}