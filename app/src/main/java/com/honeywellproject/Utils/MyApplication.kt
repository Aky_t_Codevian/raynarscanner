package com.honeywellproject.Utils

import android.app.Application
import android.content.IntentFilter
import android.net.ConnectivityManager



class MyApplication : Application() {
    val myReceiver = MyReceiver()
    override fun onCreate() {
        super.onCreate()
        instance = this

        val intentFilter = IntentFilter()
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
        registerReceiver(myReceiver, intentFilter)


        registerReceiver(myReceiver, intentFilter)

        }

    companion object {
        lateinit var instance: MyApplication
            private set
    }


    override fun onTerminate() {
        super.onTerminate()
        unregisterReceiver(myReceiver)
    }



}


