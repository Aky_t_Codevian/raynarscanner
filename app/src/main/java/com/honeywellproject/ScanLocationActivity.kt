package com.honeywellproject

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import com.honeywellproject.Utils.MyApplication

import kotlinx.android.synthetic.main.activity_scan_location.*

class ScanLocationActivity : AppCompatActivity() {

    var location:String=""
    var sharedPreferences = MyApplication.instance!!.getSharedPreferences("ryanair", Context.MODE_PRIVATE)
    var editor: SharedPreferences.Editor = sharedPreferences!!.edit()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan_location)




        et_loc.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable) {

                if(s!=null){
                    location=s.toString()
                    setLocation()
                }



            }
            override fun beforeTextChanged(s:CharSequence, start:Int,
                                           count:Int, after:Int) {}
            override fun onTextChanged(s:CharSequence, start:Int,
                                       before:Int, count:Int) {}
        })

        linear_submit.setOnClickListener {

            if(location.equals("")){

            }
            else{
                setLocation()
            }


        }
    }



    fun setLocation(){
        editor.putString("location",location)
        editor.commit()
        finish()
    }
}
