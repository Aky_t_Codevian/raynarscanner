package com.honeywellproject

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import spencerstudios.com.bungeelib.Bungee
import android.content.IntentFilter




class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)



        loadCountDown()

    }


    fun loadCountDown(){

        object : CountDownTimer(3000, 1000) {

            override fun onTick(millisUntilFinished: Long) {

            }

            override fun onFinish() {

                var intent = Intent(this@SplashActivity, LoginActivity::class.java)
                startActivity(intent)
                Bungee.fade(this@SplashActivity)
                finish()

            }
        }.start()

    }

}
